-module(queue_test).

-export([ping/0, msg/1]).
-export([stop/0, send/1, read/0]).

-define(MQ_NODE, 'gurka@Tees-MacBook-Air').
-define(OUTBOX, mq_queue_outbox).
-define(Q1, {?OUTBOX, ?MQ_NODE}).

ping() ->
    net_adm:ping(?NODE).

msg(M) ->
    ?Q1 ! M.

stop() ->
    ?Q1 ! stop.

send(Msg) ->
    ?Q1 ! {send, Msg}.

read() ->
    ?Q1 ! read.

