package org.erlang.example.messages;

import org.erlang.example.jms.QueueMgrState;

public class Stop extends GenericMessage {
    public QueueMgrState execute(QueueMgrState o) throws Exception {
	o.setStop();
	return o;
    }
}
