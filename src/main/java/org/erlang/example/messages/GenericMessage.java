package org.erlang.example.messages;

import org.erlang.example.jms.QueueMgrState;

public abstract class GenericMessage {
    abstract public QueueMgrState execute(QueueMgrState o) throws Exception;
}
