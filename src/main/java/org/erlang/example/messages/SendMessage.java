package org.erlang.example.messages;

import org.erlang.example.jms.QueueMgrState;

public class SendMessage extends GenericMessage {
    String message;
    
    public SendMessage(String message) {
	this.message = message;
    }

    public String getMessage() {
	return message;
    }
    
    public QueueMgrState execute(QueueMgrState o) throws Exception {
	o.sendQueueMessage(getMessage());
	
	return o;
    }
}
