package org.erlang.example.messages;

import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangObject;
import com.ericsson.otp.erlang.OtpErlangAtom;
import com.ericsson.otp.erlang.OtpErlangString;
import com.ericsson.otp.erlang.OtpErlangTuple;

import org.erlang.example.jms.QueueMgrState;

public class ReadAndReply extends GenericMessage {
    OtpErlangPid pid;
    
    public ReadAndReply(OtpErlangPid pid) {
	this.pid = pid;
    }

    public OtpErlangPid getPid() {
	return pid;
    }
    
    public QueueMgrState execute(QueueMgrState o) throws Exception {
	String r = o.readQueueMessage();

	System.out.println("Sending " + r + " from queue back to " + pid.toString());
	
	OtpErlangObject reply[] = new OtpErlangObject[2];
	reply[0] = new OtpErlangAtom("reply");
	reply[1] = new OtpErlangString(r);
		    
	o.sendMessage(pid, new OtpErlangTuple(reply));
	
	return o;
    }
}
