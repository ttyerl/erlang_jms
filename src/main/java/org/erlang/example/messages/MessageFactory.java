package org.erlang.example.messages;

import com.ericsson.otp.erlang.*;

public class MessageFactory {
    private static final MessageFactory instance = new MessageFactory();
    
    //private constructor to avoid client applications to use constructor
    private MessageFactory() {}

    public static MessageFactory getInstance() {
        return instance;
    }

    public GenericMessage processOtpMessage(OtpErlangObject msg) {

	OtpErlangAtom atom;
	    
	if (msg instanceof OtpErlangAtom) {
	    atom = (OtpErlangAtom) msg;
	    if (true == atom.atomValue().equals("stop")) {
		return new Stop();
	    }
	    else if (true == atom.atomValue().equals("read")) {
		return new SimpleRead();
	    }
	}
	else if (msg instanceof OtpErlangTuple) {
	    OtpErlangTuple tuple = (OtpErlangTuple) msg;
	    int arity = tuple.arity();
	    switch (arity) {
	    case 2:     // {send, Msg}
		atom = (OtpErlangAtom) tuple.elementAt(0);
		if (true == atom.atomValue().equals("send")) {
		    OtpErlangString str = (OtpErlangString) tuple.elementAt(1);
		    return new SendMessage(str.stringValue());
		}
		else if (true == atom.atomValue().equals("read")) { // {read, Pid}
		    OtpErlangPid pid = (OtpErlangPid) tuple.elementAt(1);
		    return new ReadAndReply(pid);
		}
	      
		break;
	    }
	}
	return null;
    }
}
