package org.erlang.example.messages;

import org.erlang.example.jms.QueueMgrState;

public class SimpleRead extends GenericMessage {
    public QueueMgrState execute(QueueMgrState o) throws Exception {
	String r = o.readQueueMessage();
	System.out.println("Simple Read " + r + " from queue");
	
	return o; 
    }
}
