package org.erlang.example.jms;

import com.ericsson.otp.erlang.OtpMbox;
import com.ericsson.otp.erlang.OtpErlangPid;
import com.ericsson.otp.erlang.OtpErlangObject;

import javax.jms.JMSException;
import javax.naming.NamingException;

public class QueueMgrState {
    boolean toContinue = true;
    QueueJMSAdapter adapter;
    OtpMbox mbox;

    public QueueMgrState(QueueJMSAdapter adapter, OtpMbox mbox) {
	this.adapter = adapter;
	this.mbox = mbox;
    }

    public void sendQueueMessage(String message) throws JMSException {
	adapter.sendMessage(message);
    }

    public void sendMessage(OtpErlangPid pid, OtpErlangObject o) {
	mbox.send(pid, o);
    }
    
    public String readQueueMessage() throws JMSException {
	return adapter.readMessage();
    }
    
    public boolean continueLoop() {
	return toContinue;
    }

    public void setStop() throws JMSException, NamingException {
	toContinue = false;
	adapter.stop();
    }
}
