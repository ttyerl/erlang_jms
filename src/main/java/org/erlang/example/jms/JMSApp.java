package org.erlang.example.jms;

import com.ericsson.otp.erlang.*;
import java.io.IOException;

public class JMSApp {
    OtpNode node;
    OtpMbox mbox;
    QueueMgr mgr;
    QueueMgrNode qmn;
    
    public static void main(String argv[]) {
	try {
	    JMSApp app = new JMSApp("gurka", "FRMCENPHUSJOOZBWUKTP");
	    app.createMbox("mq_queue_outbox");
	    app.start();
	}
	catch (Exception e) {
	    System.out.println("" + e);
	}
    }

    public JMSApp(String name, String cookie) throws IOException {
	qmn = new QueueMgrNode(name, cookie);
    }

    public void createMbox(String messageQueue) {
	qmn.createMBox(messageQueue);
    }
    
    public JMSApp(String name, String cookie, int i) throws IOException {
	node = new OtpNode(name, cookie);
	System.out.println("this node is " + node.node());
    }

    public void createMbox(String messageQueue, int i) {
	mgr = new QueueMgr(node, messageQueue);
    }

    public void start() throws OtpErlangExit, OtpErlangDecodeException, InterruptedException {
	//Thread thread = new Thread(mgr);
	Thread thread = new Thread(qmn);
	thread.start();
	thread.join();
	//	mgr.start();
    }
}
