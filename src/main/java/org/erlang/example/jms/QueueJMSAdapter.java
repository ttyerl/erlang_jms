package org.erlang.example.jms;

import java.util.Properties;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.jms.JMSException;
import javax.naming.NamingException;

public class QueueJMSAdapter {
    MessageProducer producer;
    MessageConsumer consumer;
    InitialContext context = null;
    Queue queue;
    Connection connection;
    Session session;

    public QueueJMSAdapter() {
	try {
	    // Step 1. Create an initial context to perform the JNDI lookup.
	    context = getContext(0);
	
	    // Step 2. Perfom a lookup on the queue
	    queue = (Queue)context.lookup("/queue/exampleQueue");

	    // Step 3. Perform a lookup on the Connection Factory
	    ConnectionFactory cf = (ConnectionFactory)context.lookup("/ConnectionFactory");

	    // Step 4.Create a JMS Connection
	    connection = cf.createConnection();

	    // Step 5. Create a JMS Session
	    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

	    // Step 6. Create a JMS Message Producer
	    producer = session.createProducer(queue);

	    // Step 9. Create a JMS Message Consumer
	    consumer = session.createConsumer(queue);
	}
	catch (Exception e) {
	    System.out.println(e);
	}
    }

    public void sendMessage(String msg) throws JMSException {
	TextMessage tm = session.createTextMessage(msg);
	producer.send(tm);
    }

    public String readMessage() throws JMSException {
	connection.start();
	TextMessage tm = (TextMessage)consumer.receive(5000);
	return tm.getText();
    }

    public void stop() throws JMSException, NamingException {
	context.close();    
	connection.close();
    }

    protected InitialContext getContext(final int serverId) throws Exception {
	Properties props = new Properties();
	props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
	props.put("java.naming.provider.url", "jnp://localhost:1099");
	props.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");

	return new InitialContext(props);
    }

}
