package org.erlang.example.jms;

import com.ericsson.otp.erlang.*;

import org.erlang.example.messages.*;

import java.io.IOException;

public class QueueMgrNode extends OtpNode implements Runnable {
    OtpMbox mbox;
    QueueJMSAdapter adapter;
    
    public QueueMgrNode(String name, String cookie) throws IOException {
	super(name, cookie);
	adapter = new QueueJMSAdapter();
    }

    public void createMBox(String messageQueue) {
	mbox = super.createMbox(messageQueue);
    }

    public void run() {
	QueueMgrState state = new QueueMgrState(adapter, mbox);
	
	while(state.continueLoop()) {
	    try {
		GenericMessage g = MessageFactory.getInstance().processOtpMessage(mbox.receive());
		state = g.execute(state);
	    }
	    catch(Exception e) {
		System.out.println("Exception: " + e);
	    }
	}
    }
}
