package org.erlang.example.jms;

import com.ericsson.otp.erlang.*;

import java.util.Properties;
import java.io.IOException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.jms.JMSException;

public class QueueMgr implements Runnable {
    OtpMbox mbox;

    // JMS
    MessageProducer producer;
    MessageConsumer consumer;
    InitialContext context = null;
    Queue queue;
    Connection connection;
    Session session;

    public QueueMgr(OtpNode node, String messageQueue) {
	try {

	    mbox = node.createMbox(messageQueue);
	    
	    // Step 1. Create an initial context to perform the JNDI lookup.
	    context = getContext(0);
	
	    // Step 2. Perfom a lookup on the queue
	    queue = (Queue)context.lookup("/queue/exampleQueue");

	    // Step 3. Perform a lookup on the Connection Factory
	    ConnectionFactory cf = (ConnectionFactory)context.lookup("/ConnectionFactory");

	    // Step 4.Create a JMS Connection
	    connection = cf.createConnection();

	    // Step 5. Create a JMS Session
	    session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

	    // Step 6. Create a JMS Message Producer
	    producer = session.createProducer(queue);

	    // Step 9. Create a JMS Message Consumer
	    consumer = session.createConsumer(queue);
	}
	catch (Exception e) {
	    System.out.println(e);
	}
    }
    
    public void sendMessage(String msg) throws JMSException {
	TextMessage tm = session.createTextMessage(msg);
	producer.send(tm);
    }

    public String readMessage() throws JMSException {
	connection.start();
	TextMessage tm = (TextMessage)consumer.receive(5000);
	return tm.getText();
    }

    public void run() {
	OtpErlangAtom atom;
	OtpErlangObject msg = null;
	boolean cont = true;

	while (cont) {
	    try {
		msg = mbox.receive();
		if (msg instanceof OtpErlangAtom) {
		    atom = (OtpErlangAtom) msg;
		    if (true == atom.atomValue().equals("stop")) {
			System.out.println("Stopping run");
			cont = false;
		    }
		    else if (true == atom.atomValue().equals("read")) {
			String fromQueue = readMessage();
			System.out.println("Read " + fromQueue + " from queue");
		    }
		}
		else {
		    processMsg(msg);
		}	
	    }
	    catch (Exception e) {
		System.out.println("Exception: " + e);
	    }
	}
    }

    private void processMsg(OtpErlangObject msg) throws Exception {
	OtpErlangAtom atom;
	OtpErlangString str;
	OtpErlangTuple tuple;
	OtpErlangPid pid;
	
	if (msg instanceof OtpErlangTuple) {
	    tuple = (OtpErlangTuple) msg;
	    int arity = tuple.arity();
	    switch (arity) {
	    case 2:     // {send, Msg}
		atom = (OtpErlangAtom) tuple.elementAt(0);
		if (true == atom.atomValue().equals("send")) {
		    str = (OtpErlangString) tuple.elementAt(1);
		    System.out.println("Sending " + str.stringValue() + " to queue");
		    sendMessage(str.stringValue());
		}
		else if (true == atom.atomValue().equals("read")) {
		    pid = (OtpErlangPid) tuple.elementAt(1);
		    String fromQueue = readMessage();
		    
		    System.out.println("Sending " + fromQueue + " from queue back to " + pid.toString());
		    OtpErlangObject reply[] = new OtpErlangObject[2];
		    reply[0] = new OtpErlangAtom("reply");
		    reply[1] = new OtpErlangString(fromQueue);
		    mbox.send(pid, new OtpErlangTuple(reply));
		}
		
		break;
	    case 3:
		arity = 3+5;
		break;
	    }
	}
	else {
	    System.out.println(msg);
	}
    }
    
    protected InitialContext getContext(final int serverId) throws Exception {
	Properties props = new Properties();
	props.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
	props.put("java.naming.provider.url", "jnp://localhost:1099");
	props.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");

	return new InitialContext(props);
    }
}
